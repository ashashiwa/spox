﻿Imports USBRC4ALPYDLL
Imports ASCOM.Astrometry.NOVAS.NOVAS3
Imports System
Imports System.IO.Ports
Public Class URBRC4ALPY
    Dim buttonn As Integer
    Dim buttons As Boolean()
    Dim mybut As Object()
    Const deltat As Double = 70
    Const maxbutton = 3
    Dim coloroff As Color
    Dim coloron As Color = Color.Green
    Private Sub All_Off_Click(sender As Object, e As EventArgs) Handles All_Off.Click
        buttonn = 0
    End Sub

    Private Sub AR_Click(sender As Object, e As EventArgs) Handles AR.Click
        buttonn = 1
    End Sub

    Private Sub NE_Click(sender As Object, e As EventArgs) Handles NE.Click
        buttonn = 2
    End Sub

    Private Sub Hg_Click(sender As Object, e As EventArgs) Handles Hg.Click
        buttonn = 3
        toggle(buttonn)
    End Sub

    Private Sub URBRC4ALPY_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'départ timer
        Timer1.Interval = 1000
        Timer1.Start()
        '
        For i = 1 To maxbutton
            buttons(i) = False
        Next
        '
        ' Get a list of serial port names.
        Dim ports As String() = SerialPort.GetPortNames()
        ListPort.Items.Clear()
        ' Display each port name to the console.
        Dim port As String
        For Each port In ports
            ListPort.Items.Add(port)
        Next port
        mybut(0) = All_Off
        mybut(1) = AR
        mybut(2) = NE
        mybut(3) = Hg
        '
        coloroff = All_Off.BackColor

    End Sub
    Private Sub reset_all()
        For i = 1 To maxbutton

        Next

    End Sub
    Private Sub toggle(btn As Integer)
        Dim status As Boolean = buttons(btn)
        status = Not (status)
        '
        buttons(buttonn) = status
        If buttons(buttonn) Then
            mybut(btn).backcolor = coloron
        Else
            mybut(btn).backcolor = coloroff
        End If
    End Sub
    Private Sub send(Msg_to_send As String)
        Dim sendusb As New USBRC4ALPYDLL.USBRC4ALPYDLL
        Dim msg As String = sendusb.Send_to_alpy( _
                              Msg_to_send _
                              )
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim novas3 As New ASCOM.Astrometry.NOVAS.NOVAS3
        Dim saveNow As DateTime = DateTime.Now
        LocalTime.Text = saveNow.TimeOfDay.ToString
        Dim JD As Double = novas3.JulianDate( _
                      (saveNow.Year), _
                      (saveNow.Month), _
                      (saveNow.Day), _
                      (3600 * saveNow.Hour + 60 * saveNow.Minute + (saveNow.Second + saveNow.Millisecond / 1000)) / (24 * 3600) _
                        )
        Dim gst As Double
        Dim err As Short = novas3.SiderealTime( _
                        Int(JD), _
                        JD - Int(JD), _
                        deltat, _
                        0, _
                        0, _
                        1, _
                        gst _
                        )
        Dim sidtime As DateTime = DateTime.FromBinary(gst)
        SideralTime.Text = JD  'sidtime.TimeOfDay.ToString
    End Sub
End Class
