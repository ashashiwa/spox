﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class URBRC4ALPY
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.All_Off = New System.Windows.Forms.Button()
        Me.AR = New System.Windows.Forms.Button()
        Me.NE = New System.Windows.Forms.Button()
        Me.Hg = New System.Windows.Forms.Button()
        Me.Local_Time = New System.Windows.Forms.Label()
        Me.Sideral_Time = New System.Windows.Forms.Label()
        Me.LocalTime = New System.Windows.Forms.Label()
        Me.SideralTime = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ListPort = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'All_Off
        '
        Me.All_Off.Location = New System.Drawing.Point(12, 123)
        Me.All_Off.Name = "All_Off"
        Me.All_Off.Size = New System.Drawing.Size(53, 26)
        Me.All_Off.TabIndex = 0
        Me.All_Off.Text = "All Off"
        Me.All_Off.UseVisualStyleBackColor = True
        '
        'AR
        '
        Me.AR.Location = New System.Drawing.Point(12, 155)
        Me.AR.Name = "AR"
        Me.AR.Size = New System.Drawing.Size(53, 26)
        Me.AR.TabIndex = 1
        Me.AR.Text = "Ar"
        Me.AR.UseVisualStyleBackColor = True
        '
        'NE
        '
        Me.NE.Location = New System.Drawing.Point(12, 187)
        Me.NE.Name = "NE"
        Me.NE.Size = New System.Drawing.Size(53, 26)
        Me.NE.TabIndex = 2
        Me.NE.Text = "Ne"
        Me.NE.UseVisualStyleBackColor = True
        '
        'Hg
        '
        Me.Hg.Location = New System.Drawing.Point(12, 219)
        Me.Hg.Name = "Hg"
        Me.Hg.Size = New System.Drawing.Size(53, 26)
        Me.Hg.TabIndex = 3
        Me.Hg.Text = "Hg"
        Me.Hg.UseVisualStyleBackColor = True
        '
        'Local_Time
        '
        Me.Local_Time.AutoSize = True
        Me.Local_Time.Location = New System.Drawing.Point(10, 10)
        Me.Local_Time.Name = "Local_Time"
        Me.Local_Time.Size = New System.Drawing.Size(62, 13)
        Me.Local_Time.TabIndex = 4
        Me.Local_Time.Text = "Local Time:"
        '
        'Sideral_Time
        '
        Me.Sideral_Time.AutoSize = True
        Me.Sideral_Time.Location = New System.Drawing.Point(9, 32)
        Me.Sideral_Time.Name = "Sideral_Time"
        Me.Sideral_Time.Size = New System.Drawing.Size(68, 13)
        Me.Sideral_Time.TabIndex = 5
        Me.Sideral_Time.Text = "Sideral Time:"
        '
        'LocalTime
        '
        Me.LocalTime.AutoSize = True
        Me.LocalTime.Location = New System.Drawing.Point(104, 9)
        Me.LocalTime.Name = "LocalTime"
        Me.LocalTime.Size = New System.Drawing.Size(73, 13)
        Me.LocalTime.TabIndex = 6
        Me.LocalTime.Text = "___________"
        '
        'SideralTime
        '
        Me.SideralTime.AutoSize = True
        Me.SideralTime.Location = New System.Drawing.Point(104, 32)
        Me.SideralTime.Name = "SideralTime"
        Me.SideralTime.Size = New System.Drawing.Size(73, 13)
        Me.SideralTime.TabIndex = 7
        Me.SideralTime.Text = "___________"
        '
        'Timer1
        '
        '
        'ListPort
        '
        Me.ListPort.FormattingEnabled = True
        Me.ListPort.Location = New System.Drawing.Point(107, 67)
        Me.ListPort.Name = "ListPort"
        Me.ListPort.Size = New System.Drawing.Size(92, 21)
        Me.ListPort.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Com Port"
        '
        'URBRC4ALPY
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ListPort)
        Me.Controls.Add(Me.SideralTime)
        Me.Controls.Add(Me.LocalTime)
        Me.Controls.Add(Me.Sideral_Time)
        Me.Controls.Add(Me.Local_Time)
        Me.Controls.Add(Me.Hg)
        Me.Controls.Add(Me.NE)
        Me.Controls.Add(Me.AR)
        Me.Controls.Add(Me.All_Off)
        Me.Name = "URBRC4ALPY"
        Me.Text = "USBRC4ALPY (c) SHELIACK"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents All_Off As System.Windows.Forms.Button
    Friend WithEvents AR As System.Windows.Forms.Button
    Friend WithEvents NE As System.Windows.Forms.Button
    Friend WithEvents Hg As System.Windows.Forms.Button
    Friend WithEvents Local_Time As System.Windows.Forms.Label
    Friend WithEvents Sideral_Time As System.Windows.Forms.Label
    Friend WithEvents LocalTime As System.Windows.Forms.Label
    Friend WithEvents SideralTime As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ListPort As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
