/*
SPOX arduino driver.
Base on serial Reader
 */

// pins for LEDs:
const int calibLed = 2;
const int tungLed = 4;
const int errorLed = 3;

const int PBCalib = 6;
const int PBTung = 5;

//Pins for Out
const int xlr1Out = 8;
const int xlr2Out = 9;

//debugging
bool debug=true;// Be carefull Can do some extra things with serial communication.

//communication. see API.
int switchNumber;
int switchState;

//true if a command is passed.
boolean commandOK=false;

//command for led blinking
boolean calibLedBlink=false;
boolean tungLedBlink=false;
boolean toggleLed=false;

//switch for reset, if PB1 and PB2 are presed together.
boolean reset=true;
boolean stringComplete=false;


String inputString;

void setup() {
  // initialize serial:
  Serial.begin(9600);

  // define the pins outputs:
  pinMode(calibLed, OUTPUT);
  pinMode(tungLed, OUTPUT);
  pinMode(errorLed, OUTPUT);
  pinMode(xlr1Out, OUTPUT);
  pinMode(xlr2Out, OUTPUT);
  
  // define the pins inputs:
  pinMode(PBCalib, INPUT);    
  pinMode(PBTung, INPUT);    
  
  //write defaults values.
  digitalWrite(xlr1Out, LOW);
  digitalWrite(xlr2Out, LOW);
  digitalWrite(tungLed, LOW);
  digitalWrite(calibLed, LOW);
  digitalWrite(errorLed, LOW);
  
  // INITIALIZE TIMER INTERRUPTS. Needed for blinking. As is, no delay in loop "loop".
  cli(); // disable global interrupts

  TCCR1A = 0; // set entire TCCR1A register to 0
  TCCR1B = 0; // same for TCCR1B

  //change here to BLINK FASTER
  OCR1A = 15624; // set compare match register to desired timer count. 16 MHz with 1024 prescaler = 15624 counts/s
  TCCR1B |= (1 << WGM12); // turn on CTC mode. clear timer on compare match

  TCCR1B |= (1 << CS10); // Set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12);

  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt

  sei(); // enable global interrupts
  
}

boolean test(String string){
    if (string.length()!=3){
        return false;
    } 
   // if ((string[0]<48)||(string[0]>50)||(string[1]<48)||(string[1]<49)){return false;}
    
    return true;
}

void loop() {
    if (debug){
        Serial.print("switchNumber ");
        Serial.println(switchNumber);
        Serial.print("switchState ");
        Serial.println(switchState);
    }
    
     if (stringComplete) {
          if (debug){
            Serial.println(inputString); 
            Serial.println(inputString.length());
          }
          
          if (test(inputString)){
              switchNumber = inputString[0]; 
              switchState = inputString[1]; 
          } else {
          Serial.println("USBRC4ALPY, invalid Query");
          }
          
          
          commandOK=true;
    // clear the string:
          inputString = "";
          stringComplete = false;
     }
     //read PB1 states
     if (digitalRead(PBCalib)==HIGH){
         if (debug){
         Serial.println("Debug : PB1 pressed");}
         switchNumber = 49;
         switchState = 49;
         commandOK=true;
         tungLedBlink=false;
         calibLedBlink=false;
     }
     
     //read PB2 states
     if (digitalRead(PBTung)==HIGH){
         if (debug){
           Serial.println("Debug : PB2 pressed");}
         switchNumber = 50;
         switchState = 49;
         commandOK=true;
         tungLedBlink=false;
         calibLedBlink=false;
         
     }
     
     if ((digitalRead(PBTung)==HIGH)&&(digitalRead(PBCalib)==HIGH)) { //signal for reset
         if (debug){Serial.println("Debug : PB1 and PB2 pressed");}
    
         switchNumber = 48;
         switchState = 48;
         commandOK=true;
             
     }
     
     
    delay(100);//change here to change responsivness.
     
   
    if (commandOK) {
      
      switch (switchNumber) {
        case 48:     
          if (switchState==48){ //48== '0'
            reset=!reset;
            if (debug){Serial.print("dark");
                          Serial.print("toogle calibLed");
                  Serial.print(calibLedBlink);
                  Serial.print("tungLed");
                  Serial.println(tungLedBlink);  
            }
            if (reset){
            //when hit 2 PB for the second time : reset ALL
                if (debug){
                  Serial.println("Reset all");
                  Serial.print("toogle calibLed");
                  Serial.print(calibLedBlink);
                  Serial.print("tungLed");
                  Serial.println(tungLedBlink);
                }    
            
                            
                digitalWrite(xlr1Out,LOW);
                digitalWrite(xlr2Out,LOW);
                digitalWrite(calibLed,LOW);
                digitalWrite(tungLed,LOW);
                tungLedBlink=false;
                calibLedBlink=false;
            }
            else{
                digitalWrite(xlr1Out,HIGH);
                digitalWrite(xlr2Out,HIGH);
                tungLedBlink=true;
                calibLedBlink=true;
            }
            
          } else {Serial.println("USBRC4ALPY, invalid Query");}
          
          break;
          
          case 49:     
          if (switchState==48){

              digitalWrite(xlr1Out,LOW);
              digitalWrite(calibLed,LOW);

          }
          else if (switchState==49) {
              digitalWrite(xlr1Out,HIGH);
              digitalWrite(calibLed,HIGH);
              digitalWrite(xlr2Out,LOW);
              digitalWrite(tungLed,LOW);
              tungLedBlink=false;
              calibLedBlink=false;
              
          }
          
//          else if (switchState==63){ //63 = ?
//              Serial.println(switchNumber+digitalRead(switch1Pin));
//          }
//          else {Serial.println("USBRC4ALPY, invalid Query");
          
          //}
          break;
          
          case 50:    // your hand is on the sensor
           if (switchState==48){
              digitalWrite(xlr2Out,LOW);
              digitalWrite(tungLed,LOW);

          }
          else if (switchState==49) {
              digitalWrite(xlr2Out,HIGH);
              digitalWrite(tungLed,HIGH);
              digitalWrite(xlr1Out,LOW);
              digitalWrite(calibLed,LOW);
              tungLedBlink=false;
              calibLedBlink=false;
          }
          break;
         
          default:
               Serial.println("USBRC4ALPY, invalid Query");
               break;
          
      } 
  }
  
  commandOK=false;
  
}
//
//void serialEvent() {
//  if(debug){
//    Serial.print("bytes to read ");
//    Serial.println(Serial.available());
//    
//    Serial.print("switchNumber ");
//    Serial.println(switchNumber);
//    Serial.print("switchState ");
//    Serial.println(switchState);
//  }  
//  
//  
////    // get the new byte:
//    byte tabReading[8];
//    int byteToRead = Serial.available();
//    for (int i=0;i++;i<byteToRead){
//    tabReading[i] = Serial.read();
//    delay(10);
//    if (tabReading[i]==10){
//    switchNumber = tabReading[i-1];
//    //    // do it again:
//    switchState = tabReading[i-2];    
//    }
//    
//    }
//    
//    
//
//
//    
//  
//}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}


// TIMER VECTOR, gets called once a second (depends on prescaler and match register)
ISR(TIMER1_COMPA_vect)
{ toggleLed=!toggleLed;
  //if (debug){Serial.println("Blink Loop");}
  if (toggleLed){
    
  if (calibLedBlink){digitalWrite(calibLed,HIGH);}
  
  
  if (tungLedBlink){digitalWrite(tungLed,HIGH);}
  }
  else {
    if (calibLedBlink){digitalWrite(calibLed,LOW);}
  
  
    if (tungLedBlink){digitalWrite(tungLed,LOW);}
  
  }
}






